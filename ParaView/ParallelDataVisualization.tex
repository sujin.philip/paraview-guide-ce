One of the goals of the \ParaView application is enabling data analysis and
visualization for large datasets. \ParaView was born out of the need for
visualizating simulation results from simulations run on supercomputing
resources that are often too big for a single desktop machine to
handle. To enable interactive visualization of such datasets, \ParaView uses
remote and/or parallel data processing. The basic concept is that if a
dataset cannot fit on a desktop machine due to memory or other
limitations, we can split the dataset among a cluster of machines,
driven from your desktop. In this chapter, we will look at the basics
of remote and parallel data processing using \ParaView. For information on
setting up clusters, please refer to the \ParaView
Wiki\cite{ParaViewServerSetup}.

\begin{didyouknow}
Remote and parallel processing are often used together, but they refer to
different concepts, and it is possible to have one without the other.\\

In the case of ParaView, remote processing refers to the concept of
having a client, typically \paraview or \pvpython, connecting to a \pvserver, which
could be running on a different, remote machine. All the data
processing and, potentially, the rendering can happen on the
\pvserver. The client drives the visualization process by
building the visualization pipeline and viewing the generated results.\\

Parallel processing refers to a concept where instead of single core
--- which we call a \texttt{rank} --- processing the entire dataset, we
split the dataset among multiple ranks. Typically, an instance of
\pvserver runs in parallel on more than one rank. If a
client is connected to a server that runs in parallel, we are using
both remote and parallel processing.\\

In the case of \pvbatch, we have an application that operates in parallel
but without a client connection.  This is a case of parallel
processing without remote processing.\\
\end{didyouknow}

\section{Understanding remote processing}

Let's consider a simple use-case. Let's say you have two computers, one located
at your office and another in your home. The one at the office is a nicer,
beefier machine with larger memory and computing capabilities than the one at
home. That being the case, you often run your simulations on the office
machine, storing the resulting files on the disk attached to your office
machine. When you're at work, to visualize those results, you simply launch
\paraview and open the data file(s). Now, what if you need to
do the visualization and data analysis from home? You have several options:
\begin{compactitem}
\item You can copy the data files over to your home machine and then use
  \paraview to visualize them. This is tedious, however, as you not only 
  have to constantly keep copying/updating your files manually, but your machine
  has poorer performance due to the decreased compute capabilities and memory
  available on it!

\item You can use a desktop sharing system like \emph{Remote Desktop} or
  \emph{VNC}, but those can be flaky depending on your network connection.
\end{compactitem}

Alternatively, you can use \ParaView's remote processing capabilities. The
concept is fairly simple. You have two separate processes: \pvserver (which runs
on your work machine) and a \paraview client (which runs on your home machine).
They communicate with each other over sockets (over an SHH tunnel, if needed).
As far as using \paraview in this mode, it's no different than how we have been
using it so far -- you create pipelines and then look at the data produced by
those pipelines in views and so on. The pipelines themselves, however, are created
remotely on the \pvserver process. Thus, the pipelines have access to the disks
on your work machine. The \ui{Open File} dialog will in fact browse the file
system on your work machine, i.e., the machine on which \pvserver is running. Any
filters that you create in your visualization pipeline execute on the
\pvserver.

While all the data processing happens on the \pvserver, when it
comes to rendering, \paraview can be configured to either do the rendering on
the server process and deliver only images to the client (remote rendering)
or to deliver the geometries to be rendered to the client and let it do the
rendering locally (local rendering).
When remote rendering, you'll be using the graphics capabilities on your work
machine (the machine running the \pvserver). Every time a new rendering needs to
be obtained (for example, when pipeline parameters are changed or you interact
with the camera, etc.), the \pvserver process will re-render a new image and
deliver that to the client. When local rendering, the geometries to be rendered
are delivered to the client and the client renders those locally. Thus, not all
interactions require server-side processing. Only when the
visualization pipeline is updated does the server need to deliver
updated geometries to the client.

%One of the main purposes of \ParaView is to allow you to create
%visualizations of large data sets that reside on remote systems
%without first bringing the data to a local machine. Transferring the
%data is often slow and wasteful of disk and network resources, and the
%visualization of large data sets can easily overwhelm the processing
%and especially memory resources of even high-performance
%workstations. To overcome these challenges, \ParaView enables processing
%data on remote resources closer to where the data resides.
%
%To see how \ParaView enables remote processing and explain where data is
%stored in the different \ParaView executables, we refer back to
%Chapter~\ref{chapter:Introduction} where we saw how to create a simple
%processing and visualization pipeline using \paraview. In this section, we
%will create that same pipeline while the \ParaView client is connected to a
%remote server. Because \ParaView was designed from the beginning to support
%remote processing, most of the steps involved in creating the pipeline
%are exactly the same as when \ParaView is used locally without being
%connected to a remote server.

\section{Remote visualization in \texttt{paraview}}
\label{sec:RemoteVisualization}

\subsection{Starting a remote server}

To begin using \ParaView for remote data processing and visualization, we must
first start the server application \pvserver on the remote system. To do this,
connect to your remote system using a shell and run:

\begin{shell}
> pvserver
\end{shell}
You will see this startup message on the terminal:
\begin{shell}
Waiting for client...
Connection URL: cs://myhost:11111
Accepting connection(s): myhost:11111
\end{shell}
This means that the server has started and is listening for a
connection from a client.

\subsection{Configuring a server connection}
To connect to this server with the
\paraview client, select \menu{File > Connect} or click
the \icon{Images/pqConnect32.png} icon in the toolbar to bring up the
\ui{Choose Server Configuration} dialog.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/Server_connect_dialog.png}
\caption{The \ui{Choose Server Configuration} dialog is used to connect
to a server.}
\label{fig:server_connect_dialog}
\end{center}
\end{figure}

\begin{commonerrors}
If your server is behind a firewall and you are attempting to connect
to it from outside the firewall, the connection may not be established
successfully. You may also try reverse connections (
Section~\ref{sec:ReverseConnections})
as a workaround for firewalls. Please consult your
network manager if you have network connection problems.
\end{commonerrors}

Figure~\ref{fig:server_connect_dialog} shows the \ui{Choose Server
Configuration} dialog with a number of entries for remote servers.  In
the figure, a number of servers have already been configured, but when
you first open this dialog, this list will be empty. Before you can
connect to a remote server, you will need to add an entry to the list
by clicking on the \ui{Add Server} button. When you do, you will see
the \ui{Edit Server Configuration} dialog as in
Figure~\ref{fig:edit_server_configuration_dialog}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/ParaView_UsersGuide_ConfigureNewServerDialog.png}
\caption{The \ui{Edit Server Configuration} dialog is used to
  configure settings for connecting to remote servers.}
\label{fig:edit_server_configuration_dialog}
\end{center}
\end{figure}

You will need to set a name for the connection, the server type, the
DNS name of the host on which you just started the server, and the
port. The default \ui{Server Type} is set to Client / Server, which
means that the server will be listening for an incoming connection
from the client. There are several other options for this setting that
we will discuss later.

When you are done, click the \ui{Configure} button. Another dialog, as
shown in Figure \ref{fig:ConfigureServerManualDialog}, will appear
where you specify how to start the server. Since we started the server
manually, we will leave the \ui{Startup Type} on the default
\ui{Manual} setting. You can optionally set the \ui{Startup Type} to
\ui{Command} and specify an external shell command to launch a server
process.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/ParaView_UsersGuide_ConfigureServerManualDialog.png}
\caption{Configure the server manually. It must be started outside of ParaView.}
\label{fig:ConfigureServerManualDialog}
\end{center}
\end{figure}

When you click the \ui{Save} button, this particular server
configuration will be saved for future use. You can go back and edit
the server configuration by selecting the entry in the list of servers
and clicking the \ui{Edit Server} button in the \ui{Choose Server
Configuration} dialog. You can delete it by clicking the \ui{Delete}
button.

Server configurations can be imported and exported through the
\ui{Choose Server Configuration} dialog. Use the \ui{Load Servers}
button to load a server configuration file and the \ui{Save Servers}
button to save a server configuration file. Files can be exchanged
with others to access the same remote servers.

\begin{didyouknow}
Visualization centers can provide system-wide server configurations on
web servers to allow non-experts to simply select an already
configured \ParaView server. The format of the XML file for saving the
server configurations is discussed online on the \ParaView Wiki at
\url{http://paraview.org/Wiki/Server\_Configuration}. These site-wide
settings can be loaded with the \ui{Fetch Servers} button.
\end{didyouknow}

\subsection{Connect to the remote server}
\label{subsec:ConnectToRemoteServer}

To connect to the server, select the server configuration you just set
up from the configuration list and click \ui{Connect}. We are now
ready to build the visualization pipelines.

\begin{commonerrors}
ParaView does not perform any kind of authentication when clients
attempt to connect to a server. For that reason, we recommend that you
do not run \pvserver on a computing resource that is open to the outside
world.\\

ParaView also does not encrypt data sent between the client and
server. If your data is sensitive, please ensure that proper network
security measures have been taken. The typical approach is to use an
SSH tunnel.
\end{commonerrors}

\subsection{Setting up a client/server visualization pipeline}

Using \paraview when connected to a remote server is not any different than when
it's being used in the default stand-alone mode. The only difference, as far as
the user interface goes, is that the \ui{Pipeline Browser} reflects the name of
the server to which you are connected. The address of the server connection next to
the \icon{Images/pqServer16.png} icon changes from \ui{builtin:}
to \ui{cs://myhost:11111}.

Since the data processing pipelines are executing on the server side, all file
I/O also happens on the server side. Hence, the \ui{Open File} dialog, when
opening a new data file, will browse the file system local to the \pvserver
executable and not the \paraview client.

%\subsection{Where does rendering occur?}
%
%When the \paraview client is connected to a server, rendering may take
%place on either the client or the server. When the memory footprint of
%the geometry representing a pipeline module is below a threshold you
%specify, the geometry will be sent to the client for local
%rendering. For large data, transferring the geometry to the client may
%take too long, or the computing resources on the client machine may be
%insufficient to store and display the data. In such cases, rendering
%takes place on the server, and rendered images are sent to the client
%for display. The geometry memory size threshold used to determine when
%rendering occurs on the client versus on the server is defined through
%the \menu{Edit > Settings} menu item on the \ui{Render View} tab in
%the \ui{Remote/Parallel Rendering Options} section.
%
%The full geometry for a pipeline module is used for rendering still
%frames when you are not interacting with the 3D scence.  If this
%geometry is smaller than the geometry memory size threshold, still
%frames will be rendered locally on the client. Otherwise, they are
%rendered on the server.
%
%A decimated version of the geometry is optionally available for
%rendering while you are interacting with the 3D scene. Decimation is a
%process where the number of vertices and faces in a geometry are
%reduced in a way that attempts to preserve shape while reducing the
%memory footprint of the geometry. If the decimated geometry is less
%than the geometry memory size threshold, it will be sent to the client
%for client-side rendering during interaction. A separate geometry
%memory size threshold that controls when the decimated geometry is
%used is available in the \ui{Interactive Rendering Options} section of
%the \ui{Render View} settings panel.
%
%The use of decimated geometry for interactive rendering and full
%geometry for still frames is meant to provide the advantages of local
%rendering (higher frame rates and lower latency when interacting) and
%remote rendering (greater large data capability) in a large number of
%cases.
%
%\subsection{Adding filters}
%
%Adding filters to the pipeline works exactly the same as
%when \ParaView is using the built-in server. To continue our example
%from Chapter~\ref{chapter:Introduction}, click on the \ui{Sphere1}
%module in the \ui{Pipeline Browser} and select \menu{Filters >
%Shrink}. When you click \ui{Apply}, the Shrink filter will execute on
%the \pvserver and, depending on the geometry memory size thresholds
%you, sends the output geometry to the \paraview client to render.
%
%\subsection{Loading remote data files}
%
%When the \ParaView client is connected to a remote server, the files
%displayed in the \ui{Open File} dialog that appear when \menu{File >
%Open} is selected are all on the file system of the remote system.  As
%in the case when \paraview is running without a remote connection, the
%reader will appear as another pipeline module.

\section{Remote visualization in \texttt{pvpython}}
\label{sec:RemoteVisualizationPython}

The \pvpython executable can be used by itself for visualization of local
data, but it can also act as a client that connects to a remote \pvserver.
Before creating a pipeline in \pvpython, use the \py{Connect} function:

\begin{python}
# Connect to remote server "myhost" on the default port, 11111
>>> Connect("myhost") # Connect to remote server "myhost" on a
                      # specified port
>>> Connect("myhost", 11111)
\end{python}

Now, when new sources are created, the data produced by the sources
will reside on the server. In the case of \pvpython, all data remains on
the server and images are generated on the server too. Images are
sent to the client for display or for saving to the local filesystem.

\section{Reverse connections}
\label{sec:ReverseConnections}

It is frequently the case that remote computing resources are located
behind a network firewall, making it difficult to connect a client
outside the firewall to a server behind it. \ParaView provides a way to
set up a \emph{reverse connection} that reverses the usual client
server roles when establishing a connection.

To use a remote connection, two steps must be performed. First,
in \paraview, a new connection must be configured with the connection type
set to reverse. To do this, open the \ui{Choose Server Configuration}
dialog through the \menu{File > Connect} menu item. Add a new
connection, setting the \ui{Name} to ``myhost (reverse)'', and select
\ui{Client / Server (reverse connection)} for \ui{Server Type}. Click
\ui{Configure}. In the \ui{Edit Server Launch Configuration} dialog that
comes up, set the \ui{Startup Type} to \ui{Manual}. Save the
configuration. Next, select this configuration and click \ui{Connect}.
A message window will appear showing that the client is awaiting a
connection from the server.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/WaitingForServerConnection.png}
\end{center}
\caption{Message window showing that the client is awaiting a connection from a server.}
\label{fig:WaitingForServerConnection}
\end{figure}

Second, \pvserver must be started with the \texttt{--reverse-connection}
(\texttt{-rc}) flag. To tell \pvserver the name of the client, set
the \texttt{--client-host} (\texttt{-ch}) command-line argument to the
hostname of the machine on which the \paraview client is running.  You
can specify a port with the \texttt{--server-port} (\texttt{-sp})
command-line argument.

\begin{shell}
pvserver -rc --client-host=mylocalhost --server-port=11111
\end{shell}

When the server starts, it prints a message indicating the success or
failure of connecting to the client. When the connection is
successful, you will see the following text in the shell:

\begin{shell}
Connecting to client (reverse connection requested)...
Connection URL: csrc://mylocalhost:11111
Client connected.
\end{shell}

To wait for reverse connections from a \pvserver in \pvpython, you use
\py{ReverseConnect} instead of \py{Connect}.

\begin{python}
# To wait for connects from a 'pvserver' on the default port
# viz. 11111
>>> ReverseConnect()

# Optionally, you can specify the port number as the argument.
>>> ReverseConnect(11111)
\end{python}

\section{Understanding parallel processing}

Parallel processing, put simply, implies processing the data in parallel,
simultaneously using multiple workers. Typically, these workers are different
processes that could be running on a multicore machine or on several nodes of a
cluster. Let's call these ranks. In most data processing and visualization
algorithms, work is directly related to the amount of data that needs to be
processed, i.e., the number of cells or points in the dataset. Thus, a straight-forward
way of distributing the work among ranks is to split an input dataset
into multiple chunks and then have each rank operate only an independent set of
chunks. Conveniently, for most algorithms, the result obtained by splitting the
dataset and processing it separately is same as the result that we'd get if we
processed the dataset in a single chunk. There are, of course, exceptions. Let's
try to understand this better with an example. For demonstration purposes,
consider this very simplified mesh.

\begin{inlinefig}
  \includegraphics[width=0.3\linewidth]{Images/ParallelExampleMesh.pdf}
\end{inlinefig}

Now, let us say we want to perform visualizations on this mesh using three
processes.  We can divide the cells of the mesh as shown below with the
blue, yellow, and pink regions.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExamplePartitions.pdf}
\end{inlinefig}

Once partitioned, some visualization algorithms will work by simply
allowing each process to independently run the algorithm on its local
collection of cells.  Take clipping as an example.  Let's say that we
define a clipping plane and give that same plane to each of the processes.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExampleClip1.pdf}
\end{inlinefig}

Each process can independently clip its cells with this plane.  The end
result is the same as if we had done the clipping serially.  If we were to
bring the cells together (which we would never actually do for large data
for obvious reasons), we would see that the clipping operation took place
correctly.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExampleClip2.pdf}
\end{inlinefig}

\subsection{Ghost levels}

Unfortunately, blindly running visualization algorithms on partitions of
cells does not always result in the correct answer.  As a simple example,
consider the \keyterm{external faces} algorithm.  The external faces
algorithm finds all cell faces that belong to only one cell, thereby,
identifying the boundaries of the mesh.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExampleExternalFaces1.pdf}
\end{inlinefig}

Oops!  We see that when all the processes ran the external faces algorithm
independently, many internal faces where incorrectly identified as being
external.  This happens where a cell in one partition has a neighbor in
another partition.  A process has no access to cells in other partitions,
so there is no way of knowing that these neighboring cells exist.

The solution employed by ParaView and other parallel visualization systems
is to use \keyterm{ghost cells}.  Ghost cells are cells that are held in
one process but actually belong to another.  To use ghost cells, we first
have to identify all the neighboring cells in each partition.  We then copy
these neighboring cells to the partition and mark them as ghost cells, as
indicated with the gray colored cells in the following example.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExampleExternalFaces2.pdf}
\end{inlinefig}

When we run the external faces algorithm with the ghost cells, we see that
we are still incorrectly identifying some internal faces as external.
However, all of these misclassified faces are on ghost cells, and the faces
inherit the ghost status of the cell from which it came.  ParaView then strips
off the ghost faces, and we are left with the correct answer.

In this example, we have shown one layer of ghost cells: only those cells
that are direct neighbors of the partition's cells.  ParaView also has the
ability to retrieve multiple layers of ghost cells, where each layer
contains the neighbors of the previous layer not already contained in a
lower ghost layer or in the original data itself.  This is useful when we have
cascading filters that each require their own layer of ghost cells.  They
each request an additional layer of ghost cells from upstream, and then
remove a layer from the data before sending it downstream.

\subsection{Data partitioning}

Since we are breaking up and distributing our data, it is prudent to
address the ramifications of how we partition the data.  The data shown in
the previous example has a \keyterm{spatially coherent} partitioning.  That
is, all the cells of each partition are located in a compact region of
space.  There are other ways to partition data.  For example, you could
have a random partitioning.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExampleRandomPartition1.pdf}
\end{inlinefig}

Random partitioning has some nice features.  It is easy to create and is
friendly to load balancing.  However, a serious problem exists with respect
to ghost cells.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/ParallelExampleRandomPartition2.pdf}
\end{inlinefig}

In this example, we see that a single level of ghost cells nearly
replicates the entire data set on all processes.  We have thus removed any
advantage we had with parallel processing.  Because ghost cells are used so
frequently, random partitioning is not used in ParaView.

\subsection{D3 Filter}

The previous section described the importance of load balancing and ghost
levels for parallel visualization.  This section describes how to achieve
that.

Load balancing and ghost cells are handled automatically by ParaView when
you are reading structured data (image data, rectilinear grid, and
structured grid).  The implicit topology makes it easy to break the data
into spatially coherent chunks and identify where neighboring cells are
located.

It is an entirely different matter when you are reading in unstructured
data (poly data and unstructured grid).  There is no implicit topology and
no neighborhood information available.  ParaView is at the mercy of how the
data was written to disk.  Thus, when you read in unstructured data, there
is no guarantee of how well-load balanced your data will be.  It is also
unlikely that the data will have ghost cells available, which means that
the output of some filters may be incorrect.

Fortunately, ParaView has a filter that will both balance your unstructured
data and create ghost cells.  This filter is called D3, which is short for
distributed data decomposition.  Using D3 is easy; simply attach the filter
(located in  \menu{Filters > Alphabetical > D3}) to whatever
data you wish to repartition.

\begin{inlinefig}
  \includegraphics[height=.3\linewidth]{Images/D3ExampleBefore.png}
  \includegraphics[height=.3\linewidth]{Images/D3ExampleAfter.png}
\end{inlinefig}

The most common use case for D3 is to attach it directly to your
unstructured grid reader.  Regardless of how well-load balanced the incoming
data might be, it is important to be able to retrieve ghost cell so that
subsequent filters will generate the correct data.  The example above shows
a cutaway of the extract surface filter on an unstructured grid.  On the
left, we see that there are many faces improperly extracted because we are
missing ghost cells.  On the right, the problem is fixed by first using the
D3 filter.

\section{ParaView architecture}

Before we see how to use ParaView for parallel data processing, let's take a
closer look at the ParaView architecture. ParaView is designed as a three-tier
client-server architecture.  The three logical units of ParaView are as follows.

\begin{description}
\item[Data Server] \keyword{data server} The unit responsible for data
  reading, filtering, and writing.  All of the pipeline objects seen in the
  pipeline browser are contained in the data server.  The data server can
  be parallel.
\item[Render Server] \keyword{render server}The unit responsible for
  rendering.  The render server can also be parallel, in which case built-in
  parallel rendering is also enabled.
\item[Client] \keyword{client}The unit responsible for establishing
  visualization.  The client controls the object creation, execution, and
  destruction in the servers, but does not contain any of the data (thus
  allowing the servers to scale without bottlenecking on the client).  If
  there is a GUI, that is also in the client.  The client is always a
  serial application.
\end{description}

These logical units need not by physically separated.  Logical units are
often embedded in the same application, removing the need for any
communication between them.  There are three modes in which you can run
ParaView.

\begin{inlinefig}
  \includegraphics[width=0.3\linewidth]{Images/RunModeStandalone.pdf}
\end{inlinefig}

The first mode, with which you are already familiar, is
\keyterm{standalone} mode.  In standalone mode, the client, data server,
and render server are all combined into a single serial application.  When
you run the \paraview application, you are automatically connected
to a \keyterm{builtin} server so that you are ready to use the full
features of ParaView.

\begin{inlinefig}
  \includegraphics[width=0.5\linewidth]{Images/RunModeClientServer.pdf}
\end{inlinefig}

The second mode is \keyterm{client-server} mode.  In client-server mode,
you execute the \pvserver program on a parallel machine and
connect to it with the \paraview client application (or \pvpython).  The
\pvserver program has both the data server and render server
embedded in it, so both data processing and rendering take place there.
The client and server are connected via a socket, which is assumed to be a
relatively slow mode of communication, so data transfer over this socket is
minimized. We saw this mode of operation in
Section~\ref{sec:RemoteVisualization}.

\begin{inlinefig}
  \includegraphics[width=0.6\linewidth]{Images/RunModeClientRenderDataServer.pdf}
\end{inlinefig}

The third mode is \keyterm{client-render server-data server} mode.  In this
mode, all three logical units are running in separate programs.  As before,
the client is connected to the render server via a single socket
connection.  The render server and data server are connected by many socket
connections, one for each process in the render server.  Data transfer over
the sockets is minimized.

Although the client-render server-data server mode is supported, we almost
never recommend using it.  The original intention of this mode is to take
advantage of heterogeneous environments where one might have a large,
powerful computational platform and a second smaller parallel machine with
graphics hardware in it.  However, in practice, we find any benefit is
almost always outstripped by the time it takes to move geometry from the
data server to the render server.  If the computational platform is much
bigger than the graphics cluster, then use software rendering on the large
computational platform.  If the two platforms are about the same size, just
perform all the computation on the graphics cluster. The executables used for
this mode are \paraview (or \pvpython) (acting as the client), \pvdataserver for
the data-server, and \pvrenderserver for the render-server.

\section{Parallel processing in \texttt{paraview} and \texttt{pvpython}}
\label{sec:ParallelProcessingParaView}

To leverage parallel processing capabilities in \paraview or \pvpython, one has
to use remote visualization, i.e., one has to connect to a \pvserver. The
processing for connecting to this \pvserver is not different from what we
say in Sections~\ref{sec:RemoteVisualization}
and~\ref{sec:RemoteVisualizationPython}. The only thing that changes is how the
\pvserver is launched.

You can start \pvserver to run on more than one processing core
using \executable{mpirun}.

\begin{shell}
mpirun -np 4 pvserver
\end{shell}

This will run \pvserver on four processing cores. It will still listen
for an incoming connection from a client on the default port. The big
difference when running \pvserver this way is that when data is loaded
from a source, it will be distributed across the four cores if the
data source is ``parallel aware'' and supports distributing the data
across the different processing cores.

To see how this data is distributed, run \pvserver as the command above and
connect to it with \paraview. Next, create another \ui{Sphere} source
using \menu{Source > Sphere}. Change the array to color by to
\ui{vtkProcessId}. You will see an image like Figure~\ref{fig:SphereColoredByProcessId}.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SphereColoredByProcessId.png}
\end{center}
\caption{Sphere source colored by \ui{vtkProcessId} array that encodes
the processing core on which the sphere data resides. Here, the sphere
data is split among the four processing cores invoked by the
command \texttt{mpirun -np 4 pvserver}.}
\label{fig:SphereColoredByProcessId}
\end{figure}

If a data reader or source is not ``parallel aware'', you can still get
the benefits of spreading the data among processing cores by using the
\ui{D3} filter. This filter partitions a dataset into convex regions
and transfers each region to a different processing core. To see an
example of how D3 partitions a data set, create a \menu{Source > Wavelet}
while \paraview is still connected to the \pvserver. Next, select
\menu{Filters > Alphabetical > D3} and click \ui{Apply}. The output of \ui{D3}
will not initially appear different from the original wavelet source.
If you color by \ui{vtkProcessId}, however, you will see the four
partitions that have been distributed to the server processing cores.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/WaveletColoredByProcessId.png}
\end{center}
\caption{Wavelet source processed by the D3 filter and colored by \ui{vtkProcessId}
array. Note how four regions of the image data are split evenly among the four
processing cores when \pvserver is run with \ui{mpirun -np 4 pvserver}.}
\label{fig:WaveletColoredByProcessId}
\end{figure}

\section{Using \texttt{pvbatch}}

In Section~\ref{sec:ParallelProcessingParaView}, we said that to use parallel
processing capabilities, one has to use remote visualization, i.e., one must use
ParaView in a client-server mode with the client (\paraview or \pvpython)
connecting to a server (\pvserver) that is being run in parallel using
\executable{mpirun}. However, there is one exception: \pvbatch. \pvpython and
\pvbatch are quite similar in that both are similar to the \executable{python}
executable that can be used to run Python scripts. The extra thing that these
executables do when compared with the standard \executable{python} is that they
initialize the environment so that any scripts that you run will be able to
locate the ParaView Python modules and libraries automatically. \pvpython is
exactly like the \paraview executable without the GUI. You can think of it as
the GUI from \paraview is replaced by a Python interpreter in \pvpython.
\pvbatch, on the other hand, can be thought of a \pvserver where, instead of
taking the control command from a remote client (\paraview or \pvpython), in
\pvbatch, the commands are taken from a Python script that is executed in the
\pvbatch executable itself. Since \pvbatch is akin to the \pvserver, unlike
\pvpython, it can be run in parallel using \executable{mpirun}. In that case,
the root rank (or the first rank or the rank with index $0$) is the one that
acts as the client, interpreting the Python script to execute the commands.
Since \pvbatch is designed to act is its own server, you cannot connect to a
remote server in the Python script, i.e., you cannot use \py{simple.Connect}.
Furthermore, \pvbatch is designed for batch operation, which means that you can only
specify the Python script as a command line argument. Unlike \pvpython, you
cannot run this executable to get an interactive shell to enter Python commands.

\begin{shell}
# process the sample.py script in single process mode.
> pvbatch sample.py

# process the sample.py script in parallel.
> mpirun -np 4 sample.py
\end{shell}

In general, you should use \pvpython if you will be using the interpreter
interactively and \pvbatch if you are running in parallel.

\begin{TODO}
  Add discussion about symmetric batch mode. That can wait, however. Not super
  critical.
\end{TODO}
