\section{Appendix}
\subsection{vtkWeakPointer, vtkSmartPointer and vtkNew}\label{appendix:objectmanagement}
To simplify reference counting, vtkWeakPointer, vtkSmartPointer and vtkNew can be used.
vtkWeakPointer stores a pointer to an object but doesn't change the reference count. When the
object gets deleted vtkWeakPointer will get reset to NULL avoiding any dangling
references. The latter two classes keep track of other vtkObjects by managing the object's
reference count. When these objects are created, they increment the reference count of the
object they are referring to and when they go out of scope, they decrement the reference count
of the object they are referring to. The following example demonstrates this.
\begin{minted}{c++}
{
  vtkNew<vtkDoubleArray> a;                 // a's ref count = 1
  a->SetName("an array");
  vtkSmartPointer<vtkPointData> pd =
    vtkSmartPointer<vtkPointData>::New();   // pd's ref count = 1
  pd->AddArray(a.GetPointer());             // a's ref count = 2
  vtkSmartPointer<vtkDoubleArray> a2 =
    vtkSmartPointer<vtkDoubleArray>::New(); // a2's ref count = 1
  pd->AddArray(a2);                         // a2's ref count = 2
  vtkWeakPointer<vtkPointData> pd2;
  pd2 = pd;                                 // pd's ref count = 1
  vtkPointData* pd3 = vtkPointData::New();
  pd2 = pd3;
  pd3->Delete();                            // pd3 is deleted
  pd2->GetClassName();                      // bug!
}                                           // don't need to call Delete on any object
\end{minted}
Note that when passing a pointer returned from vtkNew as a parameter to a method, the
GetPointer() method must be used. Other than this caveat, vtkSmartPointer and vtkNew objects
can be treated as pointers.

\subsection{ParaView Catalyst Python Script for Outputting the Full Dataset}\label{appendix:gridwriterscript}
The script below will write out the full dataset every time step for the ``input'' grid provided by
the adaptor to Catalyst. Change ``input'' on lines 7 and 40 to the appropriate identifier
for adaptors that provide multiple grids. Note that this file is available at \url{https://github.com/Kitware/ParaViewCatalystExampleCode/blob/master/SampleScripts/gridwriter.py}.

\begin{minted}{python}
from paraview.simple import *
from paraview import coprocessing

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:
      adaptorinput = coprocessor.CreateProducer( datadescription, "input" )
      grid = adaptorinput.GetClientSideObject().GetOutputDataObject(0)
      if grid.IsA("vtkImageData") or grid.IsA("vtkUniformGrid"):
        writer = coprocessor.CreateWriter( XMLPImageDataWriter, \
                                           "filename_%t.pvti", 1 )
      elif grid.IsA("vtkRectilinearGrid"):
        writer = coprocessor.CreateWriter( XMLPRectilinearGridWriter, \
                                           "filename_%t.pvtr", 1 )
      elif grid.IsA("vtkStructuredGrid"):
        writer = coprocessor.CreateWriter( XMLPStructuredGridWriter, \
                                           "filename_%t.pvts", 1 )
      elif grid.IsA("vtkPolyData"):
        writer = coprocessor.CreateWriter( XMLPPolyDataWriter, \
                                           "filename_%t.pvtp", 1 )
      elif grid.IsA("vtkUnstructuredGrid"):
        writer = coprocessor.CreateWriter( XMLPUnstructuredGridWriter, \
                                           "filename_%t.pvtu", 1 )
      elif grid.IsA("vtkUniformGridAMR"):
        writer = coprocessor.CreateWriter( XMLHierarchicalBoxDataWriter, \
                                           "filename_%t.vthb", 1 )
      elif grid.IsA("vtkMultiBlockDataSet"):
        writer = coprocessor.CreateWriter( XMLMultiBlockDataWriter, \
                                           "filename_%t.vtm", 1 )
      else:
        print "Don't know how to create a writer for a ", grid.GetClassName()

    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  freqs = {"input": [1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

coprocessor = CreateCoProcessor()
coprocessor.EnableLiveVisualization(False)

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    coprocessor.LoadRequestedData(datadescription)

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor
    coprocessor.UpdateProducers(datadescription)
    coprocessor.WriteData(datadescription)
\end{minted}

\subsection{Reusing Simulation Memory for Non-VTK Compliant Memory Layouts}\label{appendix:alternatememorylayout}
Recent work in VTK has added the ability to reuse the simulation's memory and data structures
in the co-processing pipeline.
We start with information on creating a class that derives from vtkDataArray that uses pre-allocated memory
that does not match up with VTK's expected layout. The abstract class to derive from for this
purpose is the vtkMappedDataArray. We first go through an example of this with the vtkCPExodusIIResultsArrayTemplate class
which is part of VTK.
The vtkCPExodusIIResultsArrayTemplate class is a
templated class that is a concrete implementation of vtkMappedDataArray. This class
should only be used if the data array has more than one component.
It can be used
as is if the simulation memory layout has the following constraints:
\begin{itemize}
\item The components of the data are each stored in contiguous arrays.
\item The component array data is stored in the same order as the points or cells in
the VTK dataset for point data or cell data, respectively.
\end{itemize}
If these two conditions are met then the main function of interest in this class
is:
\begin{itemize}
\item void SetExodusScalarArrays(std::vector\textless Scalar*\textgreater arrays, vtkIdType numTuples, bool save)
\end{itemize}
Here, arrays is used to pass the pointers to the beginning of each component array. The size of arrays
sets the number of components in the vtkCPExodusIIResultsArrayTemplate object. The number of tuples
is set by numTuples. Finally, if save is set to false then the object will delete the arrays
using the delete [\,] method on each component array when it is done with the memory. Otherwise it
assumes that the memory will be de-allocated elsewhere. The following code snippet demonstrates
its use.
\begin{minted}{c++}
vtkCPExodusIIResultsArrayTemplate<double>* vtkarray =
  vtkCPExodusIIResultsArrayTemplate<double>::New();
vtkarray->SetName("velocity");
std::vector<double*> simulationarrays;
simulationarrays.push_back(xvelocity);
simulationarrays.push_back(yvelocity);
simulationarrays.push_back(zvelocity);
vtkarray->SetExodusScalarArrays(myarrays, grid->GetNumberOfPoints(), true);
grid->GetPointData()->AddArray(vtkarray);
vtkarray->Delete();
\end{minted}

If the vtkCPExodusIIResultsArrayTemplate class is not appropriate for mapping
simulation memory to VTK memory, a class that derives from vtkMappedDataArray
will need to be written. The virtual methods that need to be reimplemented are (note that
Scalar is the templated data type):
\begin{itemize}
\item void Initialize()
\item void GetTuples(vtkIdList *ptIds, vtkAbstractArray *output)
\item void GetTuples(vtkIdType p1, vtkIdType p2, vtkAbstractArray *output)
\item void Squeeze()
\item vtkArrayIterator *NewIterator()
\item vtkIdType LookupValue(vtkVariant value)
\item void LookupValue(vtkVariant value, vtkIdList *ids)
\item vtkVariant GetVariantValue(vtkIdType idx)
\item void ClearLookup()
\item double* GetTuple(vtkIdType i)
\item void GetTuple(vtkIdType i, double *tuple)
\item vtkIdType LookupTypedValue(Scalar value)
\item void LookupTypedValue(Scalar value, vtkIdList *ids)
\item Scalar GetValue(vtkIdType idx)
\item Scalar\& GetValueReference(vtkIdType idx)
\item void GetTupleValue(vtkIdType idx, Scalar *t)
\end{itemize}

Since once the object is properly set up it should be considered a read-only class (i.e.
nothing in VTK should be modifying any of its contents), the following methods
should be implemented with only errors to ensure they aren't being used:
\begin{itemize}
\item int Allocate(vtkIdType sz, vtkIdType ext)
\item int Resize(vtkIdType numTuples)
\item void SetNumberOfTuples(vtkIdType number)
\item void SetTuple(vtkIdType i, vtkIdType j, vtkAbstractArray *source)
\item void SetTuple(vtkIdType i, const float *source)
\item void SetTuple(vtkIdType i, const double *source)
\item void InsertTuple(vtkIdType i, vtkIdType j, vtkAbstractArray *source)
\item void InsertTuple(vtkIdType i, const float *source)
\item void InsertTuple(vtkIdType i, const double *source)
\item void InsertTuples(vtkIdList *dstIds, vtkIdList *srcIds, vtkAbstractArray *source)
\item vtkIdType InsertNextTuple(vtkIdType j, vtkAbstractArray *source)
\item vtkIdType InsertNextTuple(const float *source)
\item vtkIdType InsertNextTuple(const double *source)
\item void DeepCopy(vtkAbstractArray *aa)
\item void DeepCopy(vtkDataArray *da)
\item void InterpolateTuple(vtkIdType i, vtkIdList *ptIndices, vtkAbstractArray* source,  double* weights)
\item void InterpolateTuple(vtkIdType i, vtkIdType id1, vtkAbstractArray *source1, vtkIdType id2, vtkAbstractArray *source2, double t)
\item void SetVariantValue(vtkIdType idx, vtkVariant value)
\item void RemoveTuple(vtkIdType id)
\item void RemoveFirstTuple()
\item void RemoveLastTuple()
\item void SetTupleValue(vtkIdType i, const Scalar *t)
\item void InsertTupleValue(vtkIdType i, const Scalar *t)
\item vtkIdType InsertNextTupleValue(const Scalar *t)
\item void SetValue(vtkIdType idx, Scalar value)
\item vtkIdType InsertNextValue(Scalar v)
\item void InsertValue(vtkIdType idx, Scalar v)
\end{itemize}

Using classes derived from vtkMappedDataArray along with any of the topologically
structured grids, the adaptor will use a negligible amount of additional memory in creating VTK data structures
representing simulation grids and fields. For vtkPolyDatas and vtkUnstructuredGrids,
the memory to store the cells can still be substantial. VTK has recently added
the vtkMappedUnstructuredGrid class as a way to do this. Since vtkUnstructuredGrids can
store all of the cells that a vtkPolyData can, we have not done the same for vtkPolyDatas though.
Using the vtkMappedUnstructuredGrid class to represent the simulation code's grid
inside of VTK is quite complex and beyond the scope of this document. For developer's interested
in using this though we refer them to \url{www.vtk.org/Wiki/VTK/InSituDataStructures}.
